import React from 'react';
import "./linear.css";
import PropTypes from 'prop-types';

const Linear = (props) => {
    return (
        <div className="linear">
            <div className='prices'>
                <p>{props.name}</p>
                <p>{props.price}<span className="dollar">$</span></p>
            </div>
            <div className="line" style={{background: "linear-gradient(to right," + props.color +" "+ props.percentage+"%, #ECECEC 0)"}}>
            </div>
        </div>
    );
}

Linear.propTypes = {
    name: PropTypes.string,
    price: PropTypes.number,
    percentage: PropTypes.number
};
export default Linear;
