import React, {useState} from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons'
import './assets/App.css';
import Header from "./components/header/Header";
import Sidebar from "./components/sidebar/Sidebar";
import Card from "./components/card/Card";
library.add(fas, faCalendarAlt)

const App = () => {
  return (
        <div className="App">
                <div className="container">
                  <Sidebar/>
                  <div className="left_part">
                      <Header/>
                      <div className="rows">
                        <Card/>
                      </div>
                  </div>
                </div>

        </div>
  );
}

export default App;
